<%@ page import="com.news.model.User" %>
<%--
    Document   : Add news
    Created on : Oct 16, 2016, 1:07:17 PM
    Author     : Howayda
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Add news</title>
<%@include file="blankStart.html"%>
<%@include file="nav.jsp"%>
    </head>
    <body>
    <% User LoggedUser;
        LoggedUser = (User) session.getAttribute("LoggedUser");
        if(LoggedUser == null) {
            response.sendRedirect("index.jsp");
        }%>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Add news</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
            <form name="submitNews" method="post" action="AddNews">
                <div class="form-group">
                    <label>Title</label>
                    <input type="text" name="title" class="form-control" rows="1"/>
                    </div>
                <div class="form-group">
                    <label>Body</label>
                    <textarea name="body" class="form-control" rows="4"></textarea>
                    </div>
                <div class="form-group">
                    <label>Category</label>
                    <select name="category" id="category" class="form-control">
                <option value="select">---Select---</option>
                <c:forEach var="myCategory" items="${categories}">
                    <option value="<c:out value="${myCategory.getCategoryID()}"/>"><c:out value="${myCategory.getCategoryName()}"/></option>
                </c:forEach>
            </select><br>
                    </div>
            Upload photo:<input type="file" name="photoNews" /><br>
                <div class="form-group">
                    <div class="checkbox">
                        <label>
            <input type="checkbox" name="privateCheckbox" value="1"/>Private<br>
            <input type="hidden" name="privateCheckbox" value="0"/>
                        </label>
                    </div>
                </div>
            <input type="submit" value="Submit" class="btn btn-success"/>
        </form>
<%@include file="blankEnd.html"%>
