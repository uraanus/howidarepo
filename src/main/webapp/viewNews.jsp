<%@ page import="com.news.model.User" %>
<%--
    Document   : getNewsByCategoryAndUserID
    Created on : Oct 21, 2016, 9:11:07 PM
    Author     : Howayda
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>View news</title>
<%@include file="blankStart.html"%>
<%@include file="nav.jsp"%>
    </head>
    <body>
    <% User LoggedUser;
        LoggedUser = (User) session.getAttribute("LoggedUser");
        if(LoggedUser == null) {
            response.sendRedirect("index.jsp");
        }%>
    <c:set var="checkEmpty" scope="request" value="${news}"/>
    <c:if test="${checkEmpty == null}">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">${newsList.get(0).getCategoryObject().getCategoryName()}</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
    <c:forEach var="news" items="${newsList}">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h3 id="grid-nesting">${news.getTitle()}</h3>
                    <c:set var="bodyLength" value="${news.getBody()}"/>
                        <c:set var="body" value="${fn:substring(bodyLength,0,50)}"/>
                    <h4>${body}</h4> <a href="DetailNews?id=${news.getId()}">See more</a>
                </div>
                <h4><a href="EditNews?id=${news.getId()}">edit</a></h4>
            </div>
        </div>
        <!-- /.col-lg-12 -->
        </div>
    </c:forEach>
    </c:if>
<%@include file="blankEnd.html"%>