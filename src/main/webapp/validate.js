/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function checkLogin()
{
    window.location = "index.jsp";
    return false;
}

function signUpValidation()
{
    var Username = document.forms["signUp"]["userName"].value;
    var password = document.forms["signUp"]["password"].value;
    var department = document.forms["signUp"]["dep"].value;
    var governorate = document.forms["signUp"]["governorate"].value;
    var role = document.forms["signUp"]["roleSelect"].value;
    var phone = document.forms["signUp"]["phone"].value;
    
    if(Username === undefined || Username === ""){
        document.getElementById('UserNameError').innerHTML="Name can not be empty!";
        return false;
    }
    if(/\s/.test(Username)){
        document.getElementById('UserNameWhitespace').innerHTML="Name can not contain whitespaces!";
        return false;
    }
    if(password === undefined || password === ""){
        document.getElementById('PasswordError').innerHTML="Password can not be empty!";
        return false;
    }
    if(/\s/.test(password)){
        document.getElementById('PasswordWhitespace').innerHTML="Password can not contain whitespaces!";
        return false;
    }
    if(phone === undefined || phone === ""){
        document.getElementById('PhoneError').innerHTML="Phone cannot be empty";
        return false;
    }
    if(!(/^\d+$/.test(phone))){
        document.getElementById('PhoneDigits').innerHTML="Phone should be numbers only";
        return false;
    }
    if(department === "select"){
        document.getElementById('DepartmentError').innerHTML="Department can not be empty";
        return false;
    }
    if(governorate === "governorateSelect"){
        document.getElementById('GovernorateError').innerHTML="Governorate cannot be empty";
        return false;
    }
    if(role === "selectedRole"){
        document.getElementById('RoleError').innerHTML="Role cannot be empty";
        return false;
    }
}
