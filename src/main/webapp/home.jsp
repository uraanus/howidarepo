<%@ page import="com.news.model.User" %>
<%--
  Created by IntelliJ IDEA.
  User: Howayda
  Date: 11/6/2016
  Time: 9:01 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Latest news</title>
<%@include file="blankStart.html"%>
<%@ include file="nav.jsp"%>
</head>
<body>
    <% User LoggedUser;
    LoggedUser = (User) session.getAttribute("LoggedUser");
    if(LoggedUser == null) {
        response.sendRedirect("index.jsp");
    }%>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Home</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
<div class="row">
<div class="col-lg-4">
    <div class="panel panel-green">
        <div class="panel-heading">
           ${latestNews.get(0).getTitle()} <span style="float: right;font-family: Arial">${latestNews.get(0).getCategoryObject().getCategoryName()}</span>
        </div>
        <div class="panel-body">
            <p>${latestNews.get(0).getBody()}</p>
        </div>
        <div class="panel-footer">
            <a href="DetailNews?id=${latestNews.get(0).getId()}">See more</a>
        </div>
    </div>
    <!-- /.col-lg-4 -->
</div>
    <div class="col-lg-4">
        <div class="panel panel-yellow">
            <div class="panel-heading">
                ${latestNews.get(1).getTitle()} <span style="float: right;font-family: Arial">${latestNews.get(1).getCategoryObject().getCategoryName()}</span>
            </div>
            <div class="panel-body">
                <p>${latestNews.get(1).getBody()}</p>
            </div>
            <div class="panel-footer">
                <a href="DetailNews?id=${latestNews.get(1).getId()}">See more</a>
            </div>
        </div>
        <!-- /.col-lg-4 -->
    </div>
    <div class="col-lg-4">
        <div class="panel panel-red">
            <div class="panel-heading">
                ${latestNews.get(2).getTitle()} <span style="float: right;font-family: Arial">${latestNews.get(2).getCategoryObject().getCategoryName()}</span>
            </div>
            <div class="panel-body">
                <p>${latestNews.get(2).getBody()}</p>
            </div>
            <div class="panel-footer">
                <a href="DetailNews?id=${latestNews.get(2).getId()}">See more</a>
            </div>
        </div>
        <!-- /.col-lg-4 -->
    </div>
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-4">
        <div class="panel panel-info">
            <div class="panel-heading">
                ${latestNews.get(3).getTitle()} <span style="float: right;font-family: Arial"> ${latestNews.get(3).getCategoryObject().getCategoryName()}</span>
            </div>
            <div class="panel-body">
                <p>${latestNews.get(3).getBody()}</p>
            </div>
            <div class="panel-footer">
                <a href="DetailNews?id=${latestNews.get(3).getId()}">See more</a>
            </div>
        </div>
    </div>
    <!-- /.col-lg-4 -->
    <div class="col-lg-4">
        <div class="panel panel-warning">
            <div class="panel-heading">
                ${latestNews.get(4).getTitle()} <span style="float: right;font-family: Arial">${latestNews.get(4).getCategoryObject().getCategoryName()}</span>
            </div>
            <div class="panel-body">
                <p>${latestNews.get(4).getBody()}</p>
            </div>
            <div class="panel-footer">
                <a href="DetailNews?id=${latestNews.get(4).getId()}">See more</a>
            </div>
        </div>
    </div>
    <!-- /.col-lg-4 -->
    <div class="col-lg-4">
        <div class="panel panel-danger">
            <div class="panel-heading">
                ${latestNews.get(5).getTitle()} <span style="float: right;font-family: Arial">${latestNews.get(5).getCategoryObject().getCategoryName()}</span>
            </div>
            <div class="panel-body">
                <p>${latestNews.get(5).getBody()}</p>
            </div>
            <div class="panel-footer">
                <a href="DetailNews?id=${latestNews.get(5).getId()}">See more</a>
            </div>
        </div>
    </div>
    <!-- /.col-lg-4 -->
</div>
<!-- /.row -->
<%@include file="blankEnd.html"%>
