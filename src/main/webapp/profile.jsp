<%@ page import="com.news.model.User" %>
<%--
    Document   : profile
    Created on : Oct 25, 2016, 2:47:40 PM
    Author     : Howayda
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Profile</title>

        <%@include file="blankStart.html"%>
        <%@include file="nav.jsp"%>
        <!-- Bootstrap Core CSS -->
        <link href="./vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

        <!-- MetisMenu CSS -->
        <link href="./vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

        <!-- DataTables CSS -->
        <link href="./vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

        <!-- DataTables Responsive CSS -->
        <link href="./vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="./dist/css/sb-admin-2.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="./vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
    <% User LoggedUser;
        LoggedUser = (User) session.getAttribute("LoggedUser");
        if(LoggedUser == null) {
            response.sendRedirect("index.jsp");
        }%>

    <div class="container-fluid">
        <div class="row">
            <form name="edit" action="SelectUser" method="post">
            <div class="col-lg-12">
                <h1 class="page-header">User profile<span style="float: right"><button type="submit" class="btn btn-default">Edit</button></span></h1>
            </div>
            <!-- /.col-lg-12 -->
            </form>
        </div>
        <!-- /.row -->
    </div>

    <div class="panel-body">
            Name: ${user.getUserName()}
            <br>Phone: ${user.getUserPhone()}
            <br>Department: ${department}
            <br>Governorate: ${governorate}
            <br>Role: ${role}
    </div>
    <!-- /.panel-body -->
    <%@include file="blankEnd.html"%>
