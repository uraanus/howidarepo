<%-- 
    Document   : home
    Created on : Sep 27, 2016, 4:23:01 PM
    Author     : Howayda
--%>

<%@page import="java.util.ArrayList"%>
<%@ page import="com.news.model.User" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin 2 - Bootstrap Admin Theme</title>

    <!-- Bootstrap Core CSS -->
    <link href="./vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="./vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="./dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="./vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<%@include file="blankStart.html"%>
<%@include file="nav.jsp"%>
</head>

<body>

<% User LoggedUser;
    LoggedUser = (User) session.getAttribute("LoggedUser");
    if(LoggedUser == null) {
        response.sendRedirect("index.jsp");
    }%>
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Home</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
        <script src="validate.js"></script>
        <%--     <form name="add" action="add.jsp" method="post">
            <input type="submit" name="add" value="add"/>
        </form> --%>

        <br><a href="ViewUser?user=${LoggedUser.getUserName()}">Profile</a>

        <form name="select" method="post" action="SelectUser" >
            <br><input type="submit" name="action" class="btn btn-success" value="Remove"/>
            <br><br><input type="submit" name="action" class="btn btn-success" value="Display"/>
        </form>
        <form name="news" method="post" action="AddNews.jsp">
            <br><input type="submit" name="addNews" class="btn btn-success" value="Add news"/>
        </form>
        <form name="signout" method="post" action="SignOut">
            <br><input type="submit" name="signOut" class="btn btn-danger" value="Log out"/>
        </form>
        <form name="latestNews" method="post" action="LatestNews">
            <br><input type="submit" name="signOut" class="btn btn-danger" value="Latest News"/>
        </form>
    </div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<%@include file="blankEnd.html"%>