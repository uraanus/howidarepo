<%@ page import="com.news.model.User" %>
<%--
    Document   : detailedNews
    Created on : Oct 28, 2016, 5:27:08 PM
    Author     : Howayda
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Detailed News</title>
<%@ include file="blankStart.html"%>
<%@include file="nav.jsp"%>
    </head>
    <body>
    <% User LoggedUser;
        LoggedUser = (User) session.getAttribute("LoggedUser");
        if(LoggedUser == null) {
            response.sendRedirect("index.jsp");
        }%>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">${myNews.getTitle()}<span style="float: right;font-family: Arial;font-size: large">${myNews.getCategoryObject().getCategoryName()}</span></h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
    <div class="col-lg-4">
        <div class="well well-lg">
            <p>${myNews.getBody()}</p>
            </div>
        </div>
<%@ include file="blankEnd.html"%>
