<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
            <li class="sidebar-search">
                <div class="input-group custom-search-form">
                    <input type="text" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                </div>
                <!-- /input-group -->
            </li>
            <li>
                <a href="LatestNews"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
            </li>
            <li>
                <a href="#"><i class="fa fa-sitemap fa-fw"></i> My news <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="ViewNews?userID=${LoggedUser.getID()}&pageID=1">Sports</a>
                    </li>
                    <li>
                        <a href="ViewNews?userID=${LoggedUser.getID()}&pageID=2">Politics</a>
                    </li>
                    <li>
                        <a href="ViewNews?userID=${LoggedUser.getID()}&pageID=3">Culture</a>
                    </li>
                    <li>
                        <a href="ViewNews?userID=${LoggedUser.getID()}&pageID=4">Arts</a>
                    </li>
                    <li>
                        <a href="ViewNews?userID=${LoggedUser.getID()}&pageID=5">Economy</a>
                    </li>
                    <li>
                        <a href="ViewNews?userID=${LoggedUser.getID()}&pageID=6">Weather</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="fa fa-sitemap fa-fw"></i> Settings <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="DepartmentTable?actionName=create">Create user</a>
                    </li>
                    <li>
                        <a href="RemoveUser"> Remove user</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="fa fa-sitemap fa-fw"></i> News <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="AddNews">Add news</a>
                    </li>
                    <li>
                        <a href="#">Pending</a>
                    </li>
                    </ul>
            </li>
        </ul>
    </div>
    <!-- /.sidebar-collapse -->
</div>
<!-- /.navbar-static-side -->
</nav>

<!-- Page Content -->
<div id="page-wrapper">