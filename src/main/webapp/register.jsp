<%@ page import="com.news.model.User" %>
<%--
    Document   : register
    Created on : Oct 5, 2016, 2:28:52 PM
    Author     : Howayda
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Sign up</title>
<%@include file="blankStart.html"%>
        </nav>
        <div id="page-wrapper">
    </head>
    <body>
        <script src="validate.js"></script>
        <% User LoggedUser;
            LoggedUser = (User) session.getAttribute("LoggedUser");
            if(LoggedUser != null) {
                response.sendRedirect("LatestNews");
            }%>
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Sign up</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <script src="validate.js"></script>
        <form name="signUp" method="post" action="SignUp" onsubmit="return signUpValidation()">
            <div class="form-group">
                <label>Name</label>
                <input type="text" name="userName" class="form-control" rows="1"/><div id="UserNameError"></div><div id="UserNameWhitespace"></div>
                </div>
            <div class="form-group">
                <label>Password</label>
                <input type="password" name="password" class="form-control" rows="1"/><div id="PasswordError"></div><div id="PasswordWhitespace"></div>
                </div>
            <div class="form-group">
                <label>Phone</label>
                <input type="text" name="phone" class="form-control" rows="1"/><div id="PhoneError"></div><div id="PhoneDigits"></div>
           </div>
            <div class="form-group">
                <label>Department</label>
                <select name="dep" id="dep" class="form-control">
                <option value="select">---Select---</option>
                <c:forEach var="row" items="${department}">
                    <option value="<c:out value="${row.getDep_id()}"/>"> <c:out value="${row.getDep_name()}"/></option>
                </c:forEach>
            </select><div id="DepartmentError"></div>
                </div>
            <div class="form-group">
                <label>Governorate</label>
                <select name="governorate" id="gov" class="form-control">
                <option value="governorateSelect">---Select---</option> 
                <c:forEach var="myGov" items="${governorate}">
                    <option value="<c:out value="${myGov.getGovernorate_id()}"/>"><c:out value="${myGov.getGetGovernorate_name()}"/></option>
                </c:forEach>
            </select><div id="GovernorateError"></div>
                </div>
            <div class="form-group">
                <label>Role</label>
                <select name="roleSelect" class="form-control">
                <option value="selectedRole">---Select</option>
                <c:forEach var="roleRows" items="${role}">
                    <option value="<c:out value="${roleRows.getRole_id()}"/>"><c:out value="${roleRows.getRole_name()}"/></option>
                </c:forEach>
            </select><div id="RoleError"></div>
                </div>
            <br><input type="submit" value="Submit"" class="btn btn-success"/>
        </form>
<%@include file="blankEnd.html"%>
