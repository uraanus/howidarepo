<%@ page import="com.news.model.User" %>
<%--
    Document   : editNewsByID
    Created on : Oct 23, 2016, 6:27:26 PM
    Author     : Howayda
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Edit news</title>
<%@include file="blankStart.html"%>
<%@include file="nav.jsp"%>
    </head>
    <body>
    <% User LoggedUser;
        LoggedUser = (User) session.getAttribute("LoggedUser");
        if(LoggedUser == null) {
            response.sendRedirect("index.jsp");
        }%>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Edit new</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
        <form name="editNews" method="post" action="EditNews">
            <div class="form-group">
                <label>Title</label>
                <textarea name="textareaTitle" class="form-control" rows="1">${myNews.getTitle()}</textarea>
                </div>
            <div class="form-group">
                <label>Body</label>
                <textarea name="textareaBody" class="form-control" rows="4">${myNews.getBody()}</textarea>

                <div class="form-group">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="checkPrivate" value="1"/>Private<br>
                            <input type="hidden" name="checkPrivate" value="0"/>
                        </label>
                    </div>
                    </div>
                    <input type="submit" value="Edit" class="btn btn-success"/>
        </form>
<%@include file="blankEnd.html"%>
