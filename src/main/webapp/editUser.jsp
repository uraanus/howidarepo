<%@ page import="com.news.model.User" %>
<%--
    Document   : editUser
    Created on : Oct 23, 2016, 9:25:43 PM
    Author     : Howayda
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Edit user</title>
        <%@include file="blankStart.html"%>
        <%@include file="nav.jsp"%>
    </head>
    <body>
    <% User LoggedUser;
        LoggedUser = (User) session.getAttribute("LoggedUser");
        if(LoggedUser == null) {
            response.sendRedirect("index.jsp");
        }%>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Edit user</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
        <form name="editUserDetails" method="post" action="EditUser">
            <div class="form-group">
                <label>Name</label>
                <textarea name="userName" class="form-control" rows="1"><c:out value="${user.getUserName()}"/></textarea>
            </div>
            <div class="form-group">
                <label>Phone</label>
                <textarea name="userPhone" class="form-control" rows="1">${user.getUserPhone()}</textarea>
            </div>
            <div class="form-group">
                <label>Department</label>
            <select name="userDepartment" class="form-control">
                <option value="${user.getUserDep()}"><c:out value="${dep_name}"/></option>
                <c:forEach var="depVar" items="${departments}">
                    <option value="<c:out value="${depVar.getDep_id()}"/>"><c:out value="${depVar.getDep_name()}"/></option>
                </c:forEach>
            </select>
            </div>
            <div class="form-group">
                <label>Governorate</label>
            <select name="userGovernorate" class="form-control">
                <option value="${user.getUserGovernorate()}"><c:out value="${gov_name}"/></option>
                <c:forEach var="govVar" items="${governorates}">
                    <option value="<c:out value="${govVar.getGovernorate_id()}"/>"><c:out value="${govVar.getGetGovernorate_name()}"/></option>
                </c:forEach>
            </select>
            </div>
            <div class="form-group">
            <label>Role</label>
            <select name="userRole" class="form-control">
                <option value="${user.getRole()}"><c:out value="${role_name}"/></option>
                <c:forEach var="roleVar" items="${roles}">
                    <option value="<c:out value="${roleVar.getRole_id()}"/>"><c:out value="${roleVar.getRole_name()}"/></option>
                </c:forEach>
            </select>
            </div>
            <input type="submit" name="submitEdit" value="Edit" class="btn btn-success"/>
        </form>
    <%@include file="blankEnd.html"%>

