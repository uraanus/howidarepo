<%@ page import="com.news.model.User" %>
<%--
  Created by IntelliJ IDEA.
  User: Howayda
  Date: 11/9/2016
  Time: 6:57 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Create user</title>
<%@include file="blankStart.html"%>
<%@include file="nav.jsp"%>
</head>
<body>
<script src="validate.js"></script>
    <% User LoggedUser;
            LoggedUser = (User) session.getAttribute("LoggedUser");
            if(LoggedUser == null) {
                response.sendRedirect("index.jsp");
            }%>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Add user</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<form name="createUser" method="post" action="AddUser" onsubmit="return signUpValidation()">
    <div class="form-group">
        <label>Name</label>
        <input type="text" name="userName" class="form-control" rows="1"/><div id="UserNameError"></div><div id="UserNameWhitespace"></div>
    </div>
    <span style="color: red">${uniqueUserError}</span>
    <div class="form-group">
        <label>Password</label>
        <input type="password" name="password" class="form-control" rows="1"/><div id="PasswordError"></div><div id="PasswordWhitespace"></div>
    </div>
    <div class="form-group">
        <label>Phone</label>
        <input type="text" name="phone" class="form-control" rows="1"/><div id="PhoneError"></div><div id="PhoneDigits"></div>
    </div>
    <div class="form-group">
        <label>Department</label>
        <select name="dep" id="dep" class="form-control">
            <option value="select">---Select---</option>
            <c:forEach var="row" items="${department}">
                <option value="<c:out value="${row.getDep_id()}"/>"> <c:out value="${row.getDep_name()}"/></option>
            </c:forEach>
        </select><div id="DepartmentError"></div>
    </div>
    <div class="form-group">
        <label>Governorate</label>
        <select name="governorate" id="gov" class="form-control">
            <option value="governorateSelect">---Select---</option>
            <c:forEach var="myGov" items="${governorate}">
                <option value="<c:out value="${myGov.getGovernorate_id()}"/>"><c:out value="${myGov.getGetGovernorate_name()}"/></option>
            </c:forEach>
        </select><div id="GovernorateError"></div>
    </div>
    <div class="form-group">
        <label>Role</label>
        <select name="roleSelect" class="form-control">
            <option value="selectedRole">---Select</option>
            <c:forEach var="roleRows" items="${role}">
                <option value="<c:out value="${roleRows.getRole_id()}"/>"><c:out value="${roleRows.getRole_name()}"/></option>
            </c:forEach>
        </select><div id="RoleError"></div>
    </div>
    <br><input type="submit" value="Create" onclick="return signUpValidation()" class="btn btn-success"/>
</form>
<%@include file="blankEnd.html"%>
