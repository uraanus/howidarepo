<%-- 
    Document   : display
    Created on : Sep 28, 2016, 1:24:26 AM
    Author     : Howayda
--%>

<%@page import="com.news.model.User"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Display</title>
    </head>
    <body>
    <% User LoggedUser;
        LoggedUser = (User) session.getAttribute("LoggedUser");
        if(LoggedUser == null) {
            response.sendRedirect("index.jsp");
        }%>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Display</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
        <script src="validate.js" ></script>
        <% User user = null; 
        user =(User)session.getAttribute("user"); %>
        name is: ${user.getUserName()}
        <br>Phone is: ${user.getUserPhone()}
        <br>Governorate is: ${user.getUserGovernorate()}
        <br>Department is: ${user.getUserDep()}
        <form method="post" action="home.jsp">
            <input type="submit" name="ok" value="OK"/>
        </form>
    </body>
</html>
