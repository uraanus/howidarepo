package com.news.controller;

import com.news.service.UserService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Howayda on 11/9/2016.
 */
@WebServlet(name = "AddUser", urlPatterns = {"/AddUser"})
public class AddUser extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String name = request.getParameter("userName");
        String password = request.getParameter("password");
        String phone = request.getParameter("phone");
        String departmentStr = request.getParameter("dep");
        Integer department = Integer.parseInt(departmentStr);
        String governorateStr = request.getParameter("governorate");
        Integer governorate = Integer.parseInt(governorateStr);
        String roleStr = request.getParameter("roleSelect");
        Integer role = Integer.parseInt(roleStr);
        UserService myUser = new UserService();
        boolean checkUnqiueUser = myUser.AddNewUser(name, password, phone, department, governorate, role);
        if(checkUnqiueUser) {
            RequestDispatcher dispatcher = request.getRequestDispatcher("LatestNews?uniqueUserError=");
            dispatcher.forward(request, response);
        }
        else {
            RequestDispatcher dispatcher = request.getRequestDispatcher("DepartmentTable");
            dispatcher.forward(request, response);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
