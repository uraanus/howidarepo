/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.news.controller;

import com.news.model.Department;
import com.news.model.Governorate;
import com.news.model.Role;
import com.news.model.User;
import com.news.service.UserService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author Howayda
 */
@WebServlet(name = "SelectUser", urlPatterns = {"/SelectUser"})
public class SelectUser extends HttpServlet {

   
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws java.io.IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("LoggedUser");
        UserService userService = new UserService();
        String dep_name = userService.getNameByIDforDifferentTables("department", user.getUserDep());
        String gov_name = userService.getNameByIDforDifferentTables("governorate", user.getUserGovernorate());
        String role_name = userService.getNameByIDforDifferentTables("role", user.getRole());
        ArrayList<Department> departments = userService.getOtherDepartments(user.getUserDep());
        ArrayList<Governorate> governorates = userService.getOtherGovernorates(user.getUserGovernorate());
        ArrayList<Role> roles = userService.getOtherRoles(user.getRole());
        request.setAttribute("user",user);
        request.setAttribute("dep_name",dep_name);
        request.setAttribute("gov_name",gov_name);
        request.setAttribute("role_name",role_name);
        request.setAttribute("departments",departments);
        request.setAttribute("governorates",governorates);
        request.setAttribute("roles",roles);
        RequestDispatcher dispatcher = request.getRequestDispatcher("editUser.jsp");
        dispatcher.forward(request, response);
}
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
