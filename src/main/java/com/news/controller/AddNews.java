/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.news.controller;

import com.news.model.Category;
import com.news.model.User;
import com.news.service.NewsService;

import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
//import org.apache.commons.fileupload.FileItem;
//import org.apache.commons.fileupload.disk.DiskFileItemFactory;
//import org.apache.commons.fileupload.servlet.ServletFileUpload;;

/**
 *
 * @author Howayda
 */
@WebServlet(name = "AddNews", urlPatterns = {"/AddNews"})
public class AddNews extends HttpServlet {
    private final String UPLOAD_DIRECTORY = "D:/";

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        NewsService newsService = new NewsService();
        ArrayList<Category> categories = new ArrayList<Category>();
        categories = newsService.getCategoryTable();
        request.setAttribute("categories",categories);
        RequestDispatcher dispatcher = request.getRequestDispatcher("AddNews.jsp");
        dispatcher.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        User myUser = (User) session.getAttribute("LoggedUser");
        int userID = myUser.getID();
        NewsService db = new NewsService();
        String title = request.getParameter("title");
        String body = request.getParameter("body");
        String category = request.getParameter("category");
        Integer catInteger = Integer.parseInt(category);
//        if(ServletFileUpload.isMultipartContent(request)){
//            try {
//                List<FileItem> multiparts = new ServletFileUpload(
//                                         new DiskFileItemFactory()).parseRequest(request);
//              
//                for(FileItem item : multiparts){
//                    if(!item.isFormField()){
//                        name = new File(item.getName()).getName();
//                        item.write( new File(UPLOAD_DIRECTORY + File.separator + name));
//                    }
//                }
//            } catch (Exception ex) {
//               ex.printStackTrace();
//            }          
//         
//        }
//         else {
//             name = "empty";
//         }
        String checkboxString = request.getParameter("privateCheckbox");
        Integer checkboxInteger = Integer.parseInt(checkboxString);
         

        db.addNews(title, body, catInteger,checkboxInteger,userID);
        RequestDispatcher dispatcher = request.getRequestDispatcher("LatestNews");
        dispatcher.forward(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
