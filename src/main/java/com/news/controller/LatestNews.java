package com.news.controller;

import com.news.model.New;
import com.news.service.NewsService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Howayda on 11/6/2016.
 */
@WebServlet(name = "LatestNews", urlPatterns = "/LatestNews")
public class LatestNews extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        NewsService news;
        ArrayList<New> latestNews = new ArrayList<New>();
        for(int i =1; i<7; i++) {
            news = new NewsService();
            latestNews.add(news.getLatestNewsByNewsCategoy(i));
        }
        request.setAttribute("latestNews",latestNews);
        RequestDispatcher dispatcher = request.getRequestDispatcher("home.jsp");
        if(dispatcher != null) {
            dispatcher.forward(request, response);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        NewsService news;
        ArrayList<New> latestNews = new ArrayList<New>();
        for(int i =1; i<7; i++) {
            news = new NewsService();
            latestNews.add(news.getLatestNewsByNewsCategoy(i));
        }
        request.setAttribute("latestNews",latestNews);
        RequestDispatcher dispatcher = request.getRequestDispatcher("home.jsp");
        if(dispatcher != null) {
            dispatcher.forward(request, response);
        }
    }
}
