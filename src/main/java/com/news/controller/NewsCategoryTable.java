package com.news.controller;

import com.news.model.Category;
import com.news.service.NewsService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Howayda on 11/3/2016.
 */
@WebServlet(name = "CategorytableServlet", urlPatterns={"/CategorytableServlet"})
public class NewsCategoryTable extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        NewsService myNews = new NewsService();
        ArrayList<Category> categories = new ArrayList<Category>();
        categories = myNews.getCategoryTable();
        session.setAttribute("categories",categories);
        RequestDispatcher dispatcher = request.getRequestDispatcher("AddNews.jsp");
        dispatcher.forward(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
