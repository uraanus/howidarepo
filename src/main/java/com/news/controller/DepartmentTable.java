package com.news.controller;

import com.news.model.Department;
import com.news.model.Governorate;
import com.news.model.Role;
import com.news.service.NewsService;
import com.news.service.UserService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Howayda on 11/2/2016.
 */
@WebServlet(name = "DepartmentTable", urlPatterns={"/DepartmentTable"})
class DepartmentTable extends HttpServlet {
        @Override
        protected void doGet (HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException {
            ArrayList<Department> dep;
            ArrayList<Governorate> gov;
            ArrayList<Role> roles;
            UserService user = new UserService();
            dep = user.getDepartmentTable();
            gov = user.getGovernorateTable();
            roles = user.getRoleTable();
            request.setAttribute("department",dep);
            request.setAttribute("governorate",gov);
            request.setAttribute("role",roles);
            String action = request.getParameter("actionName");
            if(action.equals("sign up")) {
                RequestDispatcher dispatcher = request.getRequestDispatcher("register.jsp");
                dispatcher.forward(request, response);
            }
            else if(action.equals("create")){
                RequestDispatcher dis = request.getRequestDispatcher("addUser.jsp");
                dis.forward(request, response);
            }
    }
    protected void doPost (HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
            ArrayList<Department> dep;
            ArrayList<Governorate> gov;
            ArrayList<Role> roles;
            UserService user = new UserService();
            dep = user.getDepartmentTable();
            gov = user.getGovernorateTable();
            roles = user.getRoleTable();
            request.setAttribute("department",dep);
            request.setAttribute("governorate",gov);
            request.setAttribute("role",roles);
            String uniqueError = "user name should be unique!";
            RequestDispatcher dispatcher = request.getRequestDispatcher("addUser.jsp?uniqueUserError="+uniqueError);
            dispatcher.forward(request, response);
    }
}
