/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.news.controller;

import com.news.model.User;
import com.news.service.UserService;
import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Howayda
 */
@WebServlet(name = "RemoveUser", urlPatterns = {"/RemoveUser"})
public class RemoveUser extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        ArrayList<User> allUsers = new ArrayList<User>();
        UserService user = new UserService();
        allUsers = user.getAllUsers();
        request.setAttribute("allUsers",allUsers);
        RequestDispatcher dispatcher = request.getRequestDispatcher("remove.jsp");
        dispatcher.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        UserService DB = new UserService();
        String name = (String) request.getParameter("removedName");
        String action = request.getParameter("action");
        if(action.equals("Deactivate")) {
            DB.setUserisDeletedByUserName(name,1);
            response.sendRedirect("LatestNews");
        }
        else{
            DB.setUserisDeletedByUserName(name,0);
            response.sendRedirect("LatestNews");
        }
    }
 
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
