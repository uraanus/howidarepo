/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.news.connection;

import java.sql.*;
/**
 *
 * @author Howayda
 */
public class MySqlConnection {
    static final String DRIVER = "com.mysql.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost:3306/user";
    static final String USER = "root";
    static final String PASS = "";
    Connection conn = null;
    
    public Connection OpenConnection(){
        try {
            Class.forName(DRIVER);
            conn = DriverManager.getConnection(DB_URL, USER, PASS); 
        } catch(Exception e){
            
        }
        return conn;
    }
    public void CloseConnection(Connection conn){   
        try{
        conn.close();
        } catch (Exception e){
            
        }
    }
    public static void main(String[] args) {
        MySqlConnection con = new MySqlConnection();
        Connection myConn;
        myConn = con.OpenConnection();
        if(myConn != null){
            System.out.println("hello");
        }
    }
    }
