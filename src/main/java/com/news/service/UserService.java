/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.news.service;

import com.news.connection.MySqlConnection;
import com.news.controller.AddUser;
import com.news.model.Department;
import com.news.model.Governorate;
import com.news.model.Role;
import com.news.model.User;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Howayda
 */
public class UserService {
    static final String DRIVER = "com.mysql.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost:3306/user";
    static final String USER = "root";
    static final String PASS = "";
/*********************************************************************************************************************/
    /*
    Name: authinticate()
    Description: this function makes sure that the password doesn't contains white spaces or has null value.
    Parameters: password
    Return: returns true if the password not null or doesn't contain white spaces
     */

    /**
     * *****************************************************************************************************************
     */
    public boolean authinticate(String password) {
        if (password == null || password.trim().equals("")) {
            return false;
        }
        return true;
    }
/*********************************************************************************************************************/
    /*
    Name: login()
    Description: this function validates the login process.
    Parameters: ID: the user name
                pass: the password of the user
    Return: returns object the the logged user if the login process is valid.
     */

    /**
     * *****************************************************************************************************************
     */
    public User login(String ID, String pass) {
        Connection conn = null;
        String getName = null;
        String upass = null, phone = null;
        int id = 0, user_dep = 0, user_governorate = 0, role = 0;
        User DBUSER = null;
        MySqlConnection newConn = new MySqlConnection();
        String SelectQuery = "Select * FROM user WHERE userID = ?";
        Boolean result = authinticate(pass);
        if (result) {
            try {
                conn = newConn.OpenConnection();
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                PreparedStatement preparedStatement = conn.prepareStatement(SelectQuery);
                preparedStatement.setString(1, ID);
                ResultSet rs = preparedStatement.executeQuery();
                while (rs.next()) {
                    id = rs.getInt("ID");
                    getName = rs.getString("userID");
                    upass = rs.getString("password");
                    phone = rs.getString("telephone");
                    user_dep = rs.getInt("user_dep");
                    user_governorate = rs.getInt("gov_foreignKey");
                    role = rs.getInt("role_foreignKey");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (conn != null) {
                newConn.CloseConnection(conn);
            }
            if (getName != null && upass != null) {
                if ((getName.equals(ID)) && (upass.equals(pass))) {
                    DBUSER = new User();
                    DBUSER.setUserName(getName);
                    DBUSER.setUserPassword(upass);
                    DBUSER.setID(id);
                    DBUSER.setUserPhone(phone);
                    DBUSER.setUserDep(user_dep);
                    DBUSER.setUserGovernorate(user_governorate);
                    DBUSER.setRole(role);
                }
            }
        }
        return DBUSER;
    }
/*********************************************************************************************************************/
    /*
    Name: AddNewUser()
    Description: this function adds new row into user table.
    Return: returns void.
     */

    /**
     * *****************************************************************************************************************
     */
    public boolean AddNewUser(String userName, String password, String phone, int department, int governorate, int role) {
        Connection conn = null;
        MySqlConnection createConnection = new MySqlConnection();
        String inserQuery = "INSERT INTO user (userID,password,telephone,user_dep,gov_foreignKey,role_foreignKey) VALUES(?,?,?,?,?,?)";
        conn = createConnection.OpenConnection();
        try {
            PreparedStatement insert = conn.prepareStatement(inserQuery);
            insert.setString(1, userName);
            insert.setString(2, password);
            insert.setString(3, phone);
            insert.setInt(4, department);
            insert.setInt(5, governorate);
            insert.setInt(6, role);
            insert.executeUpdate();
        } catch (SQLException ex) {
            return false;
        }
        if (conn != null) {
            createConnection.CloseConnection(conn);
        }
        return true;
    }
/*********************************************************************************************************************/
    /*
    Name: getUserByUserName()
    Description: this function retrieves row of all information about specific user using its name.
    Parameters: name: the user name
    Return: returns object of User type holds all user data.
     */

    /**
     * *****************************************************************************************************************
     */
    public User getUserByUserName(String name) {
        User user = new User();
        Connection conn = null;
        MySqlConnection myConn = new MySqlConnection();
        String userQuery = "SELECT * FROM user WHERE userID = ?";
        try {
            conn = myConn.OpenConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            PreparedStatement myPreparedStatement = conn.prepareStatement(userQuery);
            myPreparedStatement.setString(1, name);
            ResultSet rs = myPreparedStatement.executeQuery();
            while (rs.next()) {
                user.setID(rs.getInt("ID"));
                user.setUserName(rs.getString("userID"));
                user.setUserPassword(rs.getString("password"));
                user.setUserPhone(rs.getString("telephone"));
                user.setUserDep(rs.getInt("user_dep"));
                user.setUserGovernorate(rs.getInt("gov_foreignKey"));
                user.setRole(rs.getInt("role_foreignKey"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (conn != null) {
            myConn.CloseConnection(conn);
        }
        return user;
    }
/*********************************************************************************************************************/
    /*
    Name: setUserisDeletedByUserName()
    Description: this function sets isDeleted equal to 1 for the given name.
    Parameters: name: the user name to be removed.
    Return: returns void.
     */

    /**
     * *****************************************************************************************************************
     */
    public void setUserisDeletedByUserName(String name,int activationStatus) {
        Connection conn = null;
        MySqlConnection myConn = new MySqlConnection();
        String activationQuery = "UPDATE user SET isDeleted=? WHERE userID =?";
        try {
            conn = myConn.OpenConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            PreparedStatement MyPreparedStatement = conn.prepareStatement(activationQuery);
            MyPreparedStatement.setInt(1,activationStatus);
            MyPreparedStatement.setString(2, name);
            MyPreparedStatement.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        if (conn != null) {
            myConn.CloseConnection(conn);
        }
    }
/*********************************************************************************************************************/
    /*
    Name: editUserByUserID()
    Description: this function updates a row of given unique userID.
    Parameters: ID: unique userID(primaryKey), name: the new user name, phone: the new user phone, department: the nEw user department,
                governorate: the new user governorate, role: the new user role,
    Return: returns void.
     */

    /**
     * *****************************************************************************************************************
     */
    public void editUserByUserID(int ID, String name, String phone, int department, int governorate, int role) {
        Connection conn = null;
        MySqlConnection myConn = new MySqlConnection();
        String updateQuery = "UPDATE user SET userID=?,telephone=?,user_dep=?,gov_foreignKey=?, role_foreignKey=? WHERE ID=?";
        conn = myConn.OpenConnection();
        try {
            PreparedStatement preparedStatement = conn.prepareStatement(updateQuery);
            preparedStatement.setString(1, name);
            preparedStatement.setString(2, phone);
            preparedStatement.setInt(3, department);
            preparedStatement.setInt(4, governorate);
            preparedStatement.setInt(5, role);
            preparedStatement.setInt(6, ID);
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (conn != null) {
            myConn.CloseConnection(conn);
        }
    }
/*********************************************************************************************************************/
    /*
    Name: getDepartmentTable()
    Description: This function returns all rows from Department table.
    Parameters: non.
    Return: ArrayList of department holds all department table rows.
     */

    /**
     * *****************************************************************************************************************
     */
    public ArrayList<Department> getDepartmentTable() {
        Connection conn = null;
        MySqlConnection myConn = new MySqlConnection();
        Department myDep = null;
        ArrayList<Department> dep = new ArrayList<Department>();
        String query = "SELECT * from department";
        try {
            conn = myConn.OpenConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            PreparedStatement prs = conn.prepareStatement(query);
            ResultSet rs = prs.executeQuery();
            while (rs.next()) {
                myDep = new Department();
                myDep.setDep_id(rs.getInt("dep_id"));
                myDep.setDep_name(rs.getString("dep_name"));
                dep.add(myDep);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (conn != null) {
            myConn.CloseConnection(conn);
        }
        return dep;
    }
/*********************************************************************************************************************/
    /*
    Name: getGovernorateTable().
    Description: This function returns all rows from Governorate table.
    Parameters: non.
    Return: ArrayList of department holds all governorate table rows.
     */

    /**
     * *****************************************************************************************************************
     */
    public ArrayList<Governorate> getGovernorateTable() {
        Connection conn = null;
        MySqlConnection myConn = new MySqlConnection();
        String query = "SELECT * from governorate";
        Governorate myGov = null;
        ArrayList<Governorate> governorates = new ArrayList<Governorate>();
        try {
            conn = myConn.OpenConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            PreparedStatement prs = conn.prepareStatement(query);
            ResultSet rs = prs.executeQuery();
            while (rs.next()) {
                myGov = new Governorate();
                myGov.setGovernorate_id(rs.getInt("gov_id"));
                myGov.setGetGovernorate_name(rs.getString("gov_name"));
                governorates.add(myGov);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (conn != null) {
            myConn.CloseConnection(conn);
        }
        return governorates;
    }
/*********************************************************************************************************************/
    /*
    Name: getRoleTable().
    Description: This function returns all rows from Role table.
    Parameters: non.
    Return: ArrayList of department holds all Role table rows.
     */

    /**
     * *****************************************************************************************************************
     */
    public ArrayList<Role> getRoleTable() {
        Connection conn = null;
        MySqlConnection myConn = new MySqlConnection();
        Role myRole = null;
        ArrayList<Role> roles = new ArrayList<Role>();
        String query = "SELECT * from role";
        try {
            conn = myConn.OpenConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            PreparedStatement prs = conn.prepareStatement(query);
            ResultSet rs = prs.executeQuery();
            while (rs.next()) {
                myRole = new Role();
                myRole.setRole_id(rs.getInt("role_id"));
                myRole.setRole_name(rs.getString("role_name"));
                roles.add(myRole);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (conn != null) {
            myConn.CloseConnection(conn);
        }
        return roles;
    }
/*********************************************************************************************************************/
    /*
    Name: getNameByIDforDifferentTables()
    Description: This function returns the corresponding element name to the id of department,governorate or role table.
    Parameters: action: determine the required table name (ex, department, role, governorate)
               id: the id for the required string.
    Return: String contains the name of the corresponding id.
     */

    /**
     * *****************************************************************************************************************
     */
    public String getNameByIDforDifferentTables(String action, int id) {
        Connection conn = null;
        MySqlConnection myConn = new MySqlConnection();
        String name = null;
        String query;
        try {
            conn = myConn.OpenConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (action.equals("department")) {
            try {
                query = "SELECT * from department WHERE dep_id = ?";
                PreparedStatement stmt = conn.prepareStatement(query);
                stmt.setInt(1, id);
                ResultSet rs = stmt.executeQuery();
                while (rs.next()) {
                    name = rs.getString("dep_name");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (action.equals("governorate")) {
            try {
                query = "SELECT * from governorate WHERE gov_id = ?";
                PreparedStatement stmt = conn.prepareStatement(query);
                stmt.setInt(1, id);
                ResultSet rs = stmt.executeQuery();
                while (rs.next()) {
                    name = rs.getString("gov_name");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            try {
                query = "SELECT * from role WHERE role_id = ?";
                PreparedStatement stmt = conn.prepareStatement(query);
                stmt.setInt(1, id);
                ResultSet rs = stmt.executeQuery();
                while (rs.next()) {
                    name = rs.getString("role_name");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (conn != null) {
            myConn.CloseConnection(conn);
        }
        return name;
    }
/*********************************************************************************************************************/
    /*
    Name: getOtherDepartments()
    Description: This function takes department id and returns all other departments except the input parameter.
    Parameters: id: the id which will be excluded form the output.
    Return: ArrayList of department holds all other departmments.
     */

    /**
     * *****************************************************************************************************************
     */
    public ArrayList<Department> getOtherDepartments(int id) {
        Connection conn = null;
        MySqlConnection myConn = new MySqlConnection();
        Department dep;
        ArrayList<Department> departments = new ArrayList<Department>();
        String query = "SELECT * from department WHERE dep_id != ?";
        try {
            conn = myConn.OpenConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            PreparedStatement stmt = conn.prepareStatement(query);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                dep = new Department();
                dep.setDep_id(rs.getInt("dep_id"));
                dep.setDep_name(rs.getString("dep_name"));
                departments.add(dep);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (conn != null) {
            myConn.CloseConnection(conn);
        }
        return departments;
    }
/*********************************************************************************************************************/
    /*
    Name: getOtherGovernorates()
    Description: This function takes department id and returns all other governorates except the input parameter.
    Parameters: id: the id which will be excluded form the output.
    Return: ArrayList of department holds all other governorates.
     */

    /**
     * *****************************************************************************************************************
     */
    public ArrayList<Governorate> getOtherGovernorates(int id) {
        Connection conn = null;
        MySqlConnection myConn = new MySqlConnection();
        Governorate gov;
        ArrayList<Governorate> governorates = new ArrayList<Governorate>();
        String query = "SELECT * from governorate WHERE gov_id != ?";
        try {
            conn = myConn.OpenConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            PreparedStatement stmt = conn.prepareStatement(query);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                gov = new Governorate();
                gov.setGovernorate_id(rs.getInt("gov_id"));
                gov.setGetGovernorate_name(rs.getString("gov_name"));
                governorates.add(gov);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (conn != null) {
            myConn.CloseConnection(conn);
        }
        return governorates;
    }
/*********************************************************************************************************************/
    /*
    Name: getOtherRoles()
    Description: This function takes department id and returns all other roles except the input parameter.
    Parameters: id: the id which will be excluded form the output.
    Return: ArrayList of department holds all other roles.
     */

    /**
     * *****************************************************************************************************************
     */
    public ArrayList<Role> getOtherRoles(int id) {
        Connection conn = null;
        MySqlConnection myConn = new MySqlConnection();
        Role myRole = null;
        ArrayList<Role> roles = new ArrayList<Role>();
        String query = "SELECT * from role WHERE role_id != ?";
        try {
            conn = myConn.OpenConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            PreparedStatement stmt = conn.prepareStatement(query);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                myRole = new Role();
                myRole.setRole_id(rs.getInt("role_id"));
                myRole.setRole_name(rs.getString("role_name"));
                roles.add(myRole);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (conn != null) {
            myConn.CloseConnection(conn);
        }
        return roles;
    }
/*********************************************************************************************************************/
    /*
    Name: getAllUsers()
    Description: This function returns all rows from user table.
    Parameters: non.
    Return: ArrayList of User holds all non deleted user rows.
     */

    /**
     * *****************************************************************************************************************
     */
    public ArrayList<User> getAllUsers() {
        Connection conn = null;
        MySqlConnection myConn = new MySqlConnection();
        User myUser = new User();
        ArrayList<User> allUsers = new ArrayList<User>();
        String query = "SELECT * from user";
        try {
            conn = myConn.OpenConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            PreparedStatement preparedStatement = conn.prepareStatement(query);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                myUser = new User();
                myUser.setID(resultSet.getInt("ID"));
                myUser.setUserName(resultSet.getString("userID"));
                myUser.setUserDep(resultSet.getInt("user_dep"));
                myUser.setUserGovernorate(resultSet.getInt("gov_foreignKey"));
                myUser.setUserPhone(resultSet.getString("telephone"));
                myUser.setRole(resultSet.getInt("role_foreignKey"));
                myUser.setIsDeleted(resultSet.getInt("isDeleted"));
                allUsers.add(myUser);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (conn != null) {
            myConn.CloseConnection(conn);
        }
        return allUsers;
    }
/*********************************************************************************************************************/
}
