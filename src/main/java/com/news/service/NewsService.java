/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.news.service;

import com.news.connection.MySqlConnection;
import com.news.model.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Howayda
 */
public class NewsService {
/**********************************************************************************************************************/
    /*
    Name: addNews()
    Description: This function add new row into news table.
    Return type: void.
     */
/**********************************************************************************************************************/
    public void addNews(String title, String body, int category, int check, int userID) {
        Connection conn = null;
        MySqlConnection myConn = new MySqlConnection();
        String insertNews = "INSERT INTO new (title,body,newsCategory,isPrivate,userID) VALUES(?,?,?,?,?)";
        try {
            conn = myConn.OpenConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            PreparedStatement preparedStatement = conn.prepareStatement(insertNews);
            preparedStatement.setString(1, title);
            preparedStatement.setString(2, body);
            preparedStatement.setInt(3, category);
            preparedStatement.setInt(4, check);
            preparedStatement.setInt(5, userID);
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (conn != null) {
            myConn.CloseConnection(conn);
        }
    }
/**********************************************************************************************************************/
    /*
    Name: getNewsByCategoryAndUserID
    Description: This function retrieve all news for specific user according to a given category
    Parameters: getCategoryNameByCategoryID: the unique id for the category.
                userID: the name of the user who add this new.
    Return type: ArrayList of type news contain all the news added by this user of this category.
     */
 /*********************************************************************************************************************/
    public ArrayList<New> getNewsByCategoryAndUserID(int newsCategory, int userID) {
        ArrayList<New> retrivedNews = new ArrayList<New>();
        MySqlConnection myConn = new MySqlConnection();
        Connection conn = null;
        conn = myConn.OpenConnection();
        String retriveQuery = "SELECT * from new WHERE newsCategory=? AND userID=?";
        New newsModel = null;
        Category myCategory = new Category();
        try {
            PreparedStatement preparedStatement = conn.prepareStatement(retriveQuery);
            preparedStatement.setInt(1, newsCategory);
            preparedStatement.setInt(2, userID);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                newsModel = new New();
                newsModel.setId(rs.getInt("newsID"));
                newsModel.setTitle(rs.getString("title"));
                newsModel.setBody(rs.getString("body"));
                myCategory.setCategoryID(rs.getInt("newsCategory"));
                myCategory.setCategoryName(getCategoryNameByCategoryID(rs.getInt("newsCategory")));
                newsModel.setCategoryObject(myCategory);
                retrivedNews.add(newsModel);
            }
        } catch (SQLException ex) {
            Logger.getLogger(NewsService.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (conn != null) {
            myConn.CloseConnection(conn);
        }
        return retrivedNews;
    }
/*********************************************************************************************************************/
    /*
    Name: getNewsByNewsID
    Description: This function retrieve a specific row of news table according to it primaryKey(newsID).
    Parameters: id: the unique id of the news
    Return type: object of New model.
     */
 /*********************************************************************************************************************/
    public New getNewsByNewsID(int id) {
        Connection conn = null;
        MySqlConnection myConn = new MySqlConnection();
        New newsObj = null;
        Category category;
        String query = "SELECT * from new WHERE newsID=?";
        conn = myConn.OpenConnection();
        try {
            PreparedStatement preparedStatement = conn.prepareStatement(query);
            preparedStatement.setInt(1, id);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                newsObj = new New();
                category = new Category();
                newsObj.setId(rs.getInt("newsID"));
                newsObj.setTitle(rs.getString("title"));
                newsObj.setBody(rs.getString("body"));
                category.setCategoryID(rs.getInt("newsCategory"));
                category.setCategoryName(getCategoryNameByCategoryID(category.getCategoryID()));
                newsObj.setCategoryObject(category);
            }
        } catch (SQLException ex) {
            Logger.getLogger(NewsService.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (conn != null) {
            myConn.CloseConnection(conn);
        }
        return newsObj;
    }
/**********************************************************************************************************************/
    /*
    Name: editNewsByID
    Description: This function edits a certain new according to its unique id.
    Return type: void.
     */
/**********************************************************************************************************************/
    public void editNewsByID(int id, String title, String body, int checkPrivate) {
        Connection conn = null;
        MySqlConnection myConn = new MySqlConnection();
        String updateString = "UPDATE new SET title=?,body=?,isPrivate=? WHERE newsID=?";
        conn = myConn.OpenConnection();
        try {
            PreparedStatement preparedStatement = conn.prepareStatement(updateString);
            preparedStatement.setString(1, title);
            preparedStatement.setString(2, body);
            preparedStatement.setInt(3, checkPrivate);
            preparedStatement.setInt(4, id);
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(NewsService.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (conn != null) {
            myConn.CloseConnection(conn);
        }
    }
/*********************************************************************************************************************/
    /*
    Name: getCategoryNameByCategoryID
    Description: This function retrieve the corresponding string(CategoryName) of its CategoryID.
    Parameters: categoryID: the unique id of the category.
    Return type: String hold the category name.
     */
/**********************************************************************************************************************/
    public String getCategoryNameByCategoryID(int categoryID) {
        Connection conn;
        MySqlConnection myConn = new MySqlConnection();
        String query = "SELECT * from category WHERE categoryID=?";
        String categoryName = null;
        conn = myConn.OpenConnection();
        try {
            PreparedStatement preparedStatemet = conn.prepareStatement(query);
            preparedStatemet.setInt(1, categoryID);
            ResultSet rs = preparedStatemet.executeQuery();
            while (rs.next()) {
                categoryName = rs.getString("category");
            }
        } catch (SQLException ex) {
            Logger.getLogger(NewsService.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (conn != null) {
            myConn.CloseConnection(conn);
        }
        return categoryName;
    }
/**********************************************************************************************************************/
    /*
    Name: getCategoryTable
    Description: This function retrieve all rows of category table.
    Parameters: non.
    Return type: ArrayList of category holds all rows of category table.
     */
/**********************************************************************************************************************/
    public ArrayList<Category> getCategoryTable() {
        Connection conn = null;
        MySqlConnection myConn = new MySqlConnection();
        Category myCategory = null;
        ArrayList<Category> categories = new ArrayList<Category>();
        String query = "SELECT * from category";
        try {
            conn = myConn.OpenConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            PreparedStatement prs = conn.prepareStatement(query);
            ResultSet rs = prs.executeQuery();
            while (rs.next()) {
                myCategory = new Category();
                myCategory.setCategoryID(rs.getInt("categoryID"));
                myCategory.setCategoryName(rs.getString("category"));
                categories.add(myCategory);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (conn != null) {
            myConn.CloseConnection(conn);
        }
        return categories;
    }

/**********************************************************************************************************************/
    /*
    Function: getLatestNewsByNewsCategory()
    Description: this function returns object of required category parameter, contains the last added new of this category.
    Parameters: int category: the category id of the required new.
    Return: object of type new.
     */
/**********************************************************************************************************************/
    public New getLatestNewsByNewsCategoy(int category) {
        Connection conn = null;
        MySqlConnection myConn = new MySqlConnection();
        String query = "SELECT * FROM new WHERE newsCategory=? ORDER BY date DESC LIMIT 1";
        New myNews = new New();
        Category myCategory = new Category();
        try {
            conn = myConn.OpenConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            PreparedStatement prs = conn.prepareStatement(query);
            prs.setInt(1, category);
            ResultSet rs = prs.executeQuery();
            if (rs.next()) {
                myNews.setId(rs.getInt("newsID"));
                myNews.setTitle(rs.getString("title"));
                myNews.setBody(rs.getString("body"));
                myCategory.setCategoryID(rs.getInt("newsCategory"));
                myCategory.setCategoryName(getCategoryNameByCategoryID(myCategory.getCategoryID()));
                myNews.setCategoryObject(myCategory);
                myNews.setUserID(rs.getInt("userID"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (conn != null) {
            myConn.CloseConnection(conn);
        }
        return myNews;
    }
/*********************************************************************************************************************/
}
