/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.news.model;

import java.io.InputStream;

/**
 *
 * @author Howayda
 */
public class New {
    private int id;
    private String title;
    private String body;
    private Category categoryObject;
    private int userID;

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public Category getCategoryObject() {
        return categoryObject;
    }

    public int getUserID() {
        return userID;
    }

    public String getBody() {
        return body;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public void setCategoryObject(Category categoryObject) {
        this.categoryObject = categoryObject;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }
}
