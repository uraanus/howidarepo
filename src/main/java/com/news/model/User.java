/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.news.model;

/**
 *
 * @author Howayda
 */
public class User {
    private int ID;
    private String userName;
    private String userPhone;
    private String userPassword;
    private int userDep;
    private int userGovernorate;
    private int isDeleted;
    private int role;

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public int getUserDep() {
        return userDep;
    }

    public void setUserDep(int userDep) {
        this.userDep = userDep;
    }

    public int getUserGovernorate() {
        return userGovernorate;
    }

    public void setUserGovernorate(int userGovernorate) {
        this.userGovernorate = userGovernorate;
    }

    public int getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(int isDeleted) {
        this.isDeleted = isDeleted;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }
}