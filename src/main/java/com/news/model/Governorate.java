package com.news.model;

/**
 * Created by Howayda on 11/3/2016.
 */
public class Governorate {
    int governorate_id;
    String getGovernorate_name;

    public String getGetGovernorate_name() {
        return getGovernorate_name;
    }

    public void setGetGovernorate_name(String getGovernorate_name) {
        this.getGovernorate_name = getGovernorate_name;
    }

    public int getGovernorate_id() {
        return governorate_id;
    }

    public void setGovernorate_id(int governorate_id) {
        this.governorate_id = governorate_id;
    }
}
