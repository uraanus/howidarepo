package com.news.model;

/**
 * Created by Howayda on 11/3/2016.
 */
public class Role {
    Integer role_id;
    String role_name;

    public int getRole_id() {
        return role_id;
    }

    public void setRole_id(int role_id) {
        this.role_id = role_id;
    }

    public String getRole_name() {
        return role_name;
    }

    public void setRole_name(String role_name) {
        this.role_name = role_name;
    }

}
