package com.news.model;

/**
 * Created by Howayda on 11/2/2016.
 */
public class Department {
    int dep_id;
    String dep_name;

    public String getDep_name() {
        return dep_name;
    }

    public void setDep_name(String dep_name) {
        this.dep_name = dep_name;
    }

    public int getDep_id() {
        return dep_id;
    }

    public void setDep_id(Integer dep_id) {
        this.dep_id = dep_id;
    }

}
